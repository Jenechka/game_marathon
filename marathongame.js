$.fn.loadMarathonGame = function (gameName) {
  var container = this.filter('div, p').eq(0);

  // Load core files.
  MarathonGame.getCss(MarathonGame.rootDir + '/marathongame.css');

  // Load game files.
  MarathonGame.loadGameFiles(gameName, function () {
    var options = MarathonGame[gameName] || {};
    options.field = container;
    container.get(0).marathonGame = new MarathonGame(options);
  });

  return container;
}

$.fn.onEnterPressed = function (callback) {
  return this.each(function () {
    var $this = $(this);
    var bindTo;

    // Bind to element or to document.
    if ($this.is('input, textarea')) {
      bindTo = $this;
    }
    else {
      bindTo = $(document);
    }

    bindTo.bind('keypress', function (e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode == 13) {
        callback();
      }
    });
  });
}

MarathonGame.host = 'http://jenechka.loc';
MarathonGame.rootDir = MarathonGame.host + '/marathon';
MarathonGame.gamesDir = MarathonGame.rootDir + '/games';

MarathonGame.loadGameFiles = function (gameName, callback) {
  var gamePath = this.gamesDir + '/' + gameName;
  // JS
  $.getScript(gamePath + '/game.js', function () {
    // CSS
    MarathonGame.getCss(gamePath + '/game.css', callback);
  });
}

MarathonGame.getCss = function (url, callback) {
  if (document.createStyleSheet){
    document.createStyleSheet(url);
  }
  else {
    $('head').append($('<link rel="stylesheet" href="' + url + '" type="text/css" />'));
  }
  if ($.isFunction(callback)) {
    callback();
  }
}


function MarathonGame (options) {
  var _marathonGame = this;
  var object,
      previewObject,
      gamesWrapper,
      steps = [],
      players = [],
      controlPanel,
      finishedPlayers,
      currentPlayer,
      suspend;


  this.init = function () {
    this.options = options;
    this.initGamePreview();
    return this;
  }


  this.initGame = function () {
    this.initGamesWrapper();

    // Do not init game if exists.
    if (suspend) {
      this.restore();
      return this;
    }

    // DOM
    var gameId = options.gameId.replace(/[^a-z0-9]/g, '-');
    object = $('<div>').addClass('marathon-game').attr('id', gameId).appendTo(gamesWrapper);

    // Append background image.
    var img = $('<img/>').attr('src', this.options.bgImage).appendTo(object);

    this.createSteps();
    controlPanel = new CPanel(this);

    return this;
  }

  this.suspend = function () {
    suspend = true;
    gamesWrapper.hide();
    object.hide();
  }

  this.restore = function () {
    suspend = false;
    gamesWrapper.show();
    object.show();
  }

  this.initGamesWrapper = function () {
    if (!gamesWrapper) {
      gamesWrapper = $(
        '<div id="marathon-games">' +
          '<a id="marathon-games-minimize" href="#minimize"></a>' +
        '</div>' +
      '').appendTo('body');

      var docHeight = $(document).height();
      gamesWrapper.css('min-height', docHeight);

      var minimize = $('> a[href="#minimize"]', gamesWrapper);
      minimize
        .attr('title', 'Свернуть')
        .click(function () {
          _marathonGame.suspend();
          return false;
        });
    }
    return this;
  }

  this.initGamePreview = function () {
    var gameId = this.getGameId();
    var previewCssId = gameId.replace(/[^a-z0-9]/g, '-') + '-preview';

    previewObject = $(
      '<div class="marathon-game-preview">' +
        '<div class="marathon-game-bg"></div>' +
        '<div class="marathon-game-title"></div>' +
        '<div class="marathon-game-play-button"><a href="#play"></a></div>' +
      '</div>' +
    '');

    previewObject
      .attr('id', previewCssId)
      .appendTo(options.field);

    var title = $('.marathon-game-title', previewObject).text('"' + this.getGameTitle() + '"');

    var playButton = $('[href*="play"]', previewObject);
    playButton
      .attr('rel', gameId)
      .text('Начать игру')
      .click(function () {
        _marathonGame.initGame();
        return false;
      });
  }

  this.getGameId = function () {
    return this.options.gameId;
  }

  this.getGameTitle = function () {
    return this.options.gameTitle;
  }

  this.start = function (numberOfPlayers) {
    // Delete players form the previous game.
    this.deletePlayers();

    // Create plyers.
    this.createPlayers(numberOfPlayers);

    // Create empty collection of finished players..
    finishedPlayers = [];

    // Prepare players for the start.
    for (var playerNum in players) {
      players[playerNum].start();
    }

    // Randomly set current player.
    var randomNum = this.getRandomPlayerNumber();
    return this.setCurrentPlayer(randomNum);
  }

  this.restart = function () {
    var playersCount = this.countPlayers();
    return this.start(playersCount);
  }

  this.createPlayers = function (numberOfPlayers) {
    for (var i = 0; i < numberOfPlayers; i++) {
      // Create player instance.
      var player = new Player(i, this);
      // Append to collection.
      this.addPlayer(player);
    }
    return this;
  }

  this.deletePlayers = function () {
    if (players.length == 0) {
      return this;
    }

    for (var playerNum in players) {
      players[playerNum].destroy();
      delete players[playerNum];
    }
    players = [];

    return this;
  }

  this.createSteps = function () {
    var data;
    for (var num = 0; num <= this.options.lastStepNumber; num++) {
      if (this.options.stepsData && this.options.stepsData[num]) {
        data = this.options.stepsData[num];
      }
      else {
        data = {};
      }

      var step = new Step(num, data, _marathonGame);
      _marathonGame.addStep(step);
    }
    return this;
  }

  this.getObject = function () {
    return object;
  }

  this.getCPanel = function () {
    return controlPanel;
  }

  this.addStep = function (step) {
    steps.push(step);
    return this;
  }

  this.getStep = function (number) {
    return steps[number];
  }

  this.getFinishStepNumber = function () {
    return this.countSteps() - 1;
  }

  this.countSteps = function () {
    return this.options.lastStepNumber + 1;
  }

  this.addPlayer = function (player) {
    players.push(player);
    return this;
  }

  this.getPlayers = function (number) {
    return players;
  }

  this.getPlayer = function (number) {
    return players[number] || null;
  }

  this.countPlayers = function () {
    return players.length;
  }

  this.getCurrentPlayer = function () {
    return players[currentPlayer];
  }

  this.getCurrentPlayerNumber = function () {
    return currentPlayer;
  }

  this.getLastPlayerNumber = function () {
    return this.countPlayers() - 1;
  }

  this.getRandomPlayerNumber = function () {
    var min = 0; // First player number.
    var max = this.getLastPlayerNumber(); // Last player number
    return Math.floor(Math.random() * (max - min + 1)) + min; // From javascript.ru
  }

  this.setCurrentPlayer = function (number) {
    if (this.getCurrentPlayer()) {
      this.getCurrentPlayer().setNotCurrent();
    }
    currentPlayer = number;
    if (this.getPlayer(number)) {
      var player = this.getPlayer(number);
      player.setCurrent();
      controlPanel.getToolbar().setMessage('Ход перешёл игроку "' + player.getPlayerName() + '".');
    }
    return this;
  }

  this.nextPlayer = function () {
    var nextPlayerNum,
        player,
        missTurn;

    nextPlayerNum = currentPlayer;

    do {
      // Get next player number.
      nextPlayerNum = this.nextPlayerNumber(nextPlayerNum);

      // Check if player has finished the game.
      if (this.playerHasFinished(nextPlayerNum)) {
        missTurn = true;
      }
      // Ask player for missed turns.
      else {
        player = this.getPlayer(nextPlayerNum);
        missTurn = player.getTurnsToMiss() > 0;
        if (missTurn) {
          player.turnMissed();
        }
      }
    } while (missTurn);

    return this.setCurrentPlayer(nextPlayerNum);
  }

  this.nextPlayerNumber = function (_currentPlayer) {
    if (!(_currentPlayer >= 0)) {
      _currentPlayer = currentPlayer;
    }
    nextPlayerNum = _currentPlayer + 1;
    if (nextPlayerNum > (this.countPlayers() - 1)) {
      nextPlayerNum = 0;
    }
    return nextPlayerNum;
  }

  this.playerFinished = function (playerNumber) {
    // Collect finished players.
    finishedPlayers.push(playerNumber);
    // Notify player.
    var player = this.getPlayer(playerNumber);
    player.finished();
    // Play further or not.
    var playersLeft = this.countPlayers() - finishedPlayers.length;
    if (playersLeft < 2) {
      this.gameOver();
    }
    else {
      this.nextPlayer();
    }
    return this;
  }

  this.playerHasFinished = function (playerNumber) {
    return $.inArray(playerNumber, finishedPlayers) > -1;
  }

  this.gameOver = function () {
    if (finishedPlayers.length < 3) {
      // Get next player number.
      nextPlayerNum = this.nextPlayerNumber();
      // Automatically append to the finished players.
      finishedPlayers.push(nextPlayerNum);
    }

    var results = $(
      '<div class="marathon-game-results">' +
      '</div>' +
    '');

    for (var i = 0; (i < 3 && i < finishedPlayers.length); i++) {
      var place = parseInt(i) + 1;
      var player = finishedPlayers[i] + 1;
      var placedPlayer = $('<div/>').addClass('placed-player player-number-' + player + ' player-place-' + place);
      results.append(placedPlayer);
    }

    controlPanel.getMessage().confirm(results, function () {
      _marathonGame.restart();
    });

    return this;
  }

  this.validStepNumber = function(number) {
    if (number < 1) {
      number = 0;
    }
    else if (number > (this.countSteps() - 1)) {
      number = this.countSteps() - 1;
    }

    if (isNaN(number)) {
      alert('Unknown step:' + number);
      return 0;
    }

    return number;
  }


  /*
   * Control Panel class.
  */
  function CPanel(gameInstance) {
    var _CPanel = this;
    var forms = {};

    var window = {
      object: $(
        '<div class="c-panel">' +
          '<div class="c-panel-header">' +
            '<div class="c-panel-actions"><a href="#close" class="c-panel-link"></a></div>' +
            '<div class="c-panel-title"></div>' +
          '</div>' +
          '<div class="c-panel-content"></div>' +
        '</div>' +
      ''),
      init: function () {
        var _window = this;
        this.disabled = false;
        this.object.find('[href*="close"]').text('Закрыть').click(function () {
          _window.close();
          return false;
        });
        this.setTitle('Панель');
        return this;
      },
      setTitle: setTitle = function (title) {
        this.object.find('.c-panel-title').text(title);
        return this;
      },
      appendContent: function (element) {
        this.object.find('.c-panel-content').append(element);
        return this;
      },
      close: function () {
        this.object.hide();
        return this;
      },
      toggle: function () {
        this.object.toggle();
        return this;
      },
      disable: function () {
        this.disabled = true;
        this.object.addClass('c-panel-disabled');
        return this;
      },
      enable: function () {
        this.disabled = false;
        this.object.removeClass('c-panel-disabled');
        return this;
      },
      isDisabled: function () {
        return this.disabled;
      }
    };

    var toolbar = {
      object: $(
        '<div class="c-panel-toolbar">' +
          '<div class="c-panel-wrapper">' +
            '<div class="c-panel-toolbar-actions">' +
              '<a href="#toggle" class="c-panel-link"></a>' +
            '</div>' +
            '<div class="c-panel-toolbar-message">' +
              '<span class="c-panel-label">Сообщение: </span>' +
              '<span class="c-panel-value"></span>' +
            '</div>' +
          '</div>' +
        '</div>' +
      ''),
      init: function () {
        this.object.find('[href*="toggle"]').text('Скрыть/Показать').click(function () {
          window.toggle();
          return false;
        });
        return this;
      },
      setMessage: function (message) {
        this.object.find('.c-panel-toolbar-message .c-panel-value').text(message);
        return this;
      }
    };

    var message = {
      object: $(
        '<div class="c-panel-message">' +
          '<div class="c-panel-message-header">' +
            '<div class="c-panel-message-actions">' +
              '<a href="#close" class="c-panel-link"></a>' +
            '</div>' +
          '</div>' +
          '<div class="c-panel-message-content"></div>' +
        '</div>' +
      ''),
      init: function () {
        this.object
          .attr('tabindex', '')
          .hide()
          .appendTo(gameInstance.getObject())
          .onEnterPressed(function (e) {
            message.close();
          });

        // Close link.
        this.object.find('[href*="close"]').text('Закрыть').click(function () {
          message.close();
          return false;
        });
      },
      setContent: function (html) {
        if (html && html.length) {
          this.object.find('.c-panel-message-content').html(html);
        }
        return this;
      },
      confirm: function (html, callback) {
        this.object.bind('cpanelmessageclosed.cpanelconfirmclose', function () {
          $(this).unbind('cpanelmessageclosed.cpanelconfirmclose');
          if ($.isFunction(callback)) {
            callback();
          }
        });

        this.open(html);

        return this;
      },
      open: function (html) {
        this.setContent(html)._centralize();
        this.object.show().focus();
        return this;
      },
      close: function (eventName) {
        this.object.hide().blur();

        if (eventName && eventName.length) {
          this.object.trigger(eventName);
        }

        this.object.trigger('cpanelmessageclosed');

        return this;
      },
      _centralize: function () {
        this.object.show();
        var width = this.object.width();
        var height = this.object.height();
        this.object.hide();
        this.object.css({
          'margin-left': -width/2,
          'margin-top':  -height/2
        });
        return this;
      }
    }

    this.init = function () {
      var gameField = gameInstance.getObject();

      window.init().object.appendTo(gameField);
      toolbar.init().object.appendTo(gameField);
      message.init();

      return this.switchToForm('start_game');
    }

    this.getWindow = function () {
      return window;
    }

    this.getMessage = function () {
      return message;
    }

    this.getToolbar = function () {
      return toolbar;
    }

    this.switchToForm = function (id) {
      // Hide other forms.
      for (var formId in forms) {
        forms[formId].object.hide();
      }

      // Show form.
      if (forms[id]) {
        forms[id].object.show();
      }
      else {
        this.createForm(id);
      }

      // Focus first text input.
      forms[id].object.find(':text:first').focus().click();

      return this;
    }

    this.createForm = function (id) {
      // Create form.
      var form = {
        object: $('<div class="c-panel-form"></div>').addClass('c-panel-form-' + id.replace(/[^a-z0-9]/g, '-'))
      };
      // Append to panel.
      window.appendContent(form.object);
      // Append to collection
      forms[id] = form;

      switch (id) {
        case 'start_game':
          this.getToolbar().setMessage('Сколько человек будет играть? В эту игру могут играть от 2-х до 4-х человек.');

          var formItems = $(
            '<div class="c-panel-form-item">' +
              '<span class="c-panel-person"/>' +
              '<input class="c-panel-input-text" type="text" name="players_count" value=""/>' +
              '<input class="c-panel-submit" type="button" value=""/>' +
            '</div>' +
          '');

          formItems.appendTo(form.object);

          var input = formItems.find(':text').attr('placeholder', 2);
          var submit = formItems.find(':button').val('Начать игру').attr('title', 'Начать игру');

          input.onEnterPressed(function () {
            submit.click();
          })

          submit.click(function () {
            var numberOfPlayers,
                userValue = input.val();
                minPlayers = 2;
                maxPlayers = 4;

            if (userValue === '') {
              userValue = 2;
            }

            if (isNaN(userValue) || userValue < minPlayers || userValue > maxPlayers) {
              _CPanel.getToolbar().setMessage('Играть могут только от 2-х до 4-х человек!');
              input.select();
              return false;
            }

            _CPanel.switchToForm('number_from_dice');
            gameInstance.start(userValue);
            return false;
          })

          break;

        case 'number_from_dice':
          this.getToolbar().setMessage('Бросьте кубик и нажмите "Ходить" (Enter).');

          var formItems = $(
            '<div class="c-panel-form-item">' +
              '<input class="c-panel-dice" type="button" value=""/>' +
              '<input class="c-panel-input-text" type="text" name="number_from_dice" value=""/>' +
              '<input class="c-panel-submit" type="button" value=""/>' +
            '</div>' +
            '<div class="c-panel-form-item">' +
              '<a href="#restart_game" class="c-panel-link" />' +
              '<a href="#new_game" class="c-panel-link" />' +
            '</div>' +
          '');

          formItems.appendTo(form.object);

          var dice = formItems.find('.c-panel-dice').val('Бросить кубик').attr('title', 'Бросить кубик');
          var input = formItems.find(':text').val('');
          var submit = formItems.find('.c-panel-submit').val('Ходить').attr('title', 'Ходить');
          var restartGame = formItems.find('[href*="restart_game"]').text('Начать заново');
          var newGame = formItems.find('[href*="new_game"]').text('Новая игра');

          dice.click(function (e) {
            var number = _CPanel.rollDice();
            input.val(number).trigger('valuechange').click();
          });

          input
            .bind('click', function () {
              $(this).select();
            })
            .bind('keyup', function (e) {
              var keyCode = e.keyCode || e.which;
              if (keyCode != 13) {
                $(this).trigger('valuechange');
              }
            })
            .bind('valuechange', function () {
              var value = $(this).val();
              if (value > 0) {
                _CPanel.getToolbar().setMessage('На кубике выпало число "' + value + '".');
              }
            })
            .onEnterPressed(function () {
              submit.click();
            });

          submit.click(function () {
            // Submit once per player.
            if (_CPanel.getWindow().isDisabled()) {
              return false;
            }

            // Validate number from the dice.
            var value = parseInt(input.val());
            if (isNaN(value) || value < 1 || value > 6) {
              _CPanel.getToolbar().setMessage('Кубик только один! Число должно быть от 1 до 6.');
              input.select();
              return false;
            }

            // Move player.
            _CPanel.getWindow().disable();
            gameInstance.getCurrentPlayer().move(value, function () {
              _CPanel.getWindow().enable();
              input.val('').click();
            });

            return false;
          });

          restartGame.click(function () {
            if (confirm('Вы уверены, что хотите начать игру заново?')) {
              gameInstance.restart();
              _CPanel.switchToForm('number_from_dice');
            }
            return false;
          });

          newGame.click(function () {
            if (confirm('Вы уверены, что хотите начать новую игру?')) {
              _CPanel.switchToForm('start_game');
            }
            return false;
          });

          break;
      }

      return this;
    }

    this.rollDice = function (dices) {
      dices = (dices > 1) ? dices : 1;
      var min = 1;
      var max = dices * 6;
      return Math.floor(Math.random() * (max - min + 1)) + min; // From javascript.ru
    }

    return this.init();
  }


  /*
   * Step class.
  */
  function Step(number, data, gameInstance) {
    var _Step = this;
    var object = $('<div class="step"></div>'),
        edge;

    this.init = function () {
      object
        .addClass('step-' + number)
        .appendTo(gameInstance.getObject());

      if (number == 0) {
        edge = 'start';
      }
      else if (number == (gameInstance.countSteps() - 1)) {
        edge = 'finish';
      }

      if (edge) {
        object.addClass('step-' + edge);
      }

      return this;
    }

    this.getObject = function () {
      return object;
    }

    this.getGame = function () {
      return gameInstance;
    }

    this.getEdge = function () {
      return edge;
    }

    this.getNumber = function () {
      return number;
    }

    this.unpackData = function (player, callback) {
      var action, message;

      // Execute action if any.
      if (data.action && !this.getEdge()) {
        switch (data.action) {
          case 'turn':
            var operation = data.actionArgs[0]; // + or -
            var turns = data.actionArgs[1]; // Number of turns.

            if (operation == '+') {
              message = data.message || 'Вы получаете дополнительно ходов: ' + turns;
              action = function () {
                _Step.showMessage(message, callback);
              }
            }
            else if (operation == '-') {
              message = data.message || 'Вы должны пропустить ходов: ' + turns;
              action = function () {
                player.missTurns(turns);
                _Step.showMessage(message, function () {
                  // Callback.
                  if ($.isFunction(callback)) {
                    callback();
                  }
                  gameInstance.nextPlayer();
                });
              }
            }
            break;

          case 'jump':
            var toStep = data.actionArgs[0]; // Step index.
            message = data.message || 'Вы передвигаетесь на номер ' + toStep;
            action = function () {
              _Step.showMessage(message, function () {
                player.jumpToStep(toStep, callback);
              });
            }
            break;
        }

        if ($.isFunction(action)) {
          action();
        }
      }
      // Finish step action.
      else if (this.getEdge() == 'finish') {
        var afterUnpack = function () {
          // Callback.
          if ($.isFunction(callback)) {
            callback();
          }

          var playerNum = player.getPlayerNumber();
          gameInstance.playerFinished(playerNum);
        }

        if (data.message) {
          this.showMessage(data.message, afterUnpack);
        }
        else {
          afterUnpack();
        }
      }
      // Default step behavior.
      else {
        var afterUnpack = function () {
          // Callback.
          if ($.isFunction(callback)) {
            callback();
          }

          // Switch to next player.
          if (_Step.getEdge() != 'start') {
            gameInstance.nextPlayer();
          }
        }

        if (data.message) {
          this.showMessage(data.message, afterUnpack);
        }
        else {
          afterUnpack();
        }
      }

      return this;
    }

    this.showMessage = function (message, callback) {
      gameInstance.getCPanel().getMessage().confirm(message, callback);
    }

    return this.init();
  }


  /*
   * Player class.
  */
  function Player(playerNumber, gameInstance) {
    var _Player = this;
    var object = $('<div class="player">'),
        current,
        blinking,
        waiting,
        walking,
        onStep,
        missTurns,
        finished;

    this.init = function () {
      object
        .addClass('player-' + this.getPlayerHumanNumber())
        .attr('title', this.getPlayerName())
        .bind('playerstatechanged playercreated', function () {
          // Current or not.
          if (_Player.isCurrent()) {
            object.addClass('player-current');
          }
          else {
            object.removeClass('player-current');
          }

          // Waiting or walking.
          if (_Player.isWaiting()) {
            object.removeClass('player-walking').addClass('player-waiting');
          }
          else {
            object.removeClass('player-waiting').addClass('player-walking');
          }

          // Blink or not.
          if (_Player.isCurrent() && _Player.isWaiting()) {
            _Player.blink();
          }
          else {
            _Player.stopBlinking();
          }

          // Finished or not.
          if (_Player.hasFinished()) {
            object.addClass('player-finished');
          }
          else {
            object.removeClass('player-finished');
          }
        });

      return this.start();
    }

    this.start = function () {
      missTurns = 0;
      finished = false;

      this.jumpToStep(0);
      this.setNotCurrent();
      return this;
    }

    this.destroy = function () {
      object.remove();
    }

    this.triggerEvent = function (e) {
      object.trigger(e);
      return this;
    }

    this.setCurrent = function () {
      current = true;
      this.waiting();
      return this.triggerEvent('playerstatechanged');
    }

    this.setNotCurrent = function () {
      current = false;
      this.waiting();
      return this.triggerEvent('playerstatechanged');
    }

    this.isCurrent = function () {
      return current;
    }

    this.waiting = function (trigger) {
      walking = false;
      waiting = true;
      if (trigger) {
        return this.triggerEvent('playerstatechanged');
      }
      return this;
    }

    this.isWaiting = function () {
      return waiting;
    }

    this.walking = function () {
      waiting = false;
      walking = true;
      return this.triggerEvent('playerstatechanged');
    }

    this.isWalking = function () {
      return walking;
    }

    this.missTurns = function (turns) {
      missTurns = turns;
      return this;
    }

    this.getTurnsToMiss = function () {
      return missTurns;
    }

    this.turnMissed = function () {
      missTurns--;
      this.setNotCurrent();
      return this;
    }

    this.blink = function (speed, blinks) {
      if (blinking) {
        return this;
      }

      speed = speed || 300;
      var calls = 0;

      blinking = setInterval(function () {
        object.animate({opacity:0.6}, speed).animate({opacity:1}, speed, function () {calls++;});
        if (blinks && calls >= blinks) {
          _Player.stopBlinking();
        }
      }, speed * 2);

      return this;
    }

    this.stopBlinking = function () {
      if (blinking) {
        clearInterval(blinking);
        blinking = null;
      }
      return this;
    }

    this.jumpToStep = function (number, callback, preventUnpack) {
      var step;
      number = gameInstance.validStepNumber(number);
      step = gameInstance.getStep(number);
      object.appendTo(step.getObject());
      this.setCurrentStep(number);
      // Unpack step data.
      if (!preventUnpack) {
        step.unpackData(this, callback);
      }
      return this;
    }

    this.goToStep = function (number, callback) {
      number = gameInstance.validStepNumber(number);
      var stepDelay = 700;
      var curDelay = stepDelay;

      this.walking();

      // Extend callback.
      var extendedCallback = function () {
        _Player.waiting(true);
        // Evaluate given callback.
        if ($.isFunction(callback)) {
          callback();
        }
      }

      for (var i = this.getCurrentStep() + 1; i <= number; i++) {
        setTimeout(function (player, number, callback, lastStepNumber) {
          var _callback, preventUnpack = true;
          if (number == lastStepNumber) {
            _callback = callback;
            preventUnpack = false;
          }
          player.jumpToStep(number, _callback, preventUnpack);
        }, curDelay, this, i, extendedCallback, number);
        curDelay += stepDelay;
      }

      return this;
    }

    this.move = function (steps, callback) {
      var endStep = this.getCurrentStep() + parseInt(steps);
      return this.goToStep(endStep, callback);
    }

    this.getPlayerNumber = function () {
      return playerNumber;
    }

    this.getPlayerHumanNumber = function () {
      return playerNumber + 1;
    }

    this.getPlayerName = function () {
      return 'Игрок ' + this.getPlayerHumanNumber();
    }

    this.getGame = function () {
      return gameInstance;
    }

    this.getObject = function () {
      return object;
    }

    this.setCurrentStep = function (number) {
      onStep = parseInt(number);
      return this;
    }

    this.getCurrentStep = function () {
      return onStep;
    }

    this.finished = function () {
      finished = true;
      return this.triggerEvent('playerstatechanged');
    }

    this.hasFinished = function () {
      return finished;
    }

    return this.init();
  }


  return this.init();
}
